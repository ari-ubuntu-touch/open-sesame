var client;
var pub = false;


function settings() {
    if (document.getElementById("settings").style.display == "none") {
        document.getElementById("settings").style.display = "block";
    } else {
        document.getElementById("settings").style.display = "none";
    }
}

function usermsg(msg) {
    console.log("user message")
    var newout = document.createElement("p");
    newout.innerText = msg;
    var output = document.getElementById("output");
    output.appendChild(newout);

    setTimeout(function() {
        document.getElementById('output').removeChild(document.getElementById('output').getElementsByTagName('p')[0]);
    }, 5000); 
}

function publish() {
    // Get options from html
    var ptopic = document.getElementById("ptopic").value;
    var command = document.getElementById("command").value;

    if (pub == true) {
        pub = false;
        console.log("publishing...");
        console.log(ptopic);
        console.log(command);
        client.publish(ptopic, command);
        usermsg("Published");
        // client.end();
    }
}

function connect () {
    usermsg("Connecting...")

    // Get options from html 
    var clientId = document.getElementById("clientId").value;
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var protocol = document.getElementById("protocol").value;
    var broker = document.getElementById("broker").value;
    var port = document.getElementById("port").value;

    console.log("Setting used:")
    console.log(clientId);
    console.log(username);
    console.log(password);
    console.log(protocol);
    console.log(broker);
    console.log(port);


    // connection option
    const options = {
  		  clean: true, // retain session
        connectTimeout: 4000, // Timeout period
        // Authentication information
        clientId: clientId,
        username: username,
        password: password,
    }

    // const connectUrl = 'wss://io.adafruit.com:443'
    const connectUrl = protocol + "://" + broker + ":" + port;
    console.log("Testing this username:")
    console.log(connectUrl)
    client = mqtt.connect(connectUrl, options)


    client.on('connect', () => {
        usermsg("Connected!");
        // publish();
    })

    client.on('reconnect', (error) => {
        usermsg("Error. Reconnecting...")
        console.log('reconnecting:', error)
    })

    client.on('error', (error) => {
        console.log('Connection failed:', error)
    })

    client.on('message', (topic, message) => {
        console.log('receive message：', topic, message.toString())
    })
}

function openSesame() {
    usermsg("Opening...");
    pub = true;
    publish();
}

function getData() {
    document.getElementById("clientId").value = localStorage.getItem("clientId")
    document.getElementById("username").value = localStorage.getItem("username")
    document.getElementById("password").value = localStorage.getItem("password")
    document.getElementById("protocol").value = localStorage.getItem("protocol")
    document.getElementById("broker").value = localStorage.getItem("broker")
    document.getElementById("port").value = localStorage.getItem("port")
    document.getElementById("ptopic").value = localStorage.getItem("ptopic")
    document.getElementById("command").value = localStorage.getItem("command")
}

function saveData() {
    // Get data
    var clientId = document.getElementById("clientId").value;
    var username = document.getElementById("username").value;
    var password = document.getElementById("password").value;
    var protocol = document.getElementById("protocol").value;
    var broker = document.getElementById("broker").value;
    var port = document.getElementById("port").value;
    var ptopic = document.getElementById("ptopic").value;
    var command = document.getElementById("command").value;

    // Save data
    localStorage.setItem("clientId", clientId)
    localStorage.setItem("username", username);
    localStorage.setItem("password", password);
    localStorage.setItem("protocol", protocol);
    localStorage.setItem("broker", broker);
    localStorage.setItem("port", port);
    localStorage.setItem("ptopic", ptopic);
    localStorage.setItem("command", command);
}
