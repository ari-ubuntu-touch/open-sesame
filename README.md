# Open Sesame

MQTT client

mqtt.js wrapper

Has a single button which publishes to a single topic.

Intended as a simple garage door opener.

Works with:

 - wss
 - ws
 - mqtt
 - mqtts
 - wxs
 - alis

[![OpenStore](https://open-store.io/badges/en_US.svg)](https://open-store.io/app/opensesame.red)

## License

Copyright (C) 2022  red

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
